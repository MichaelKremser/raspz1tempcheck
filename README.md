# raspz1tempcheck

These scripts run on my Raspberry Pi Zero WH so I can see the temperature and humidity it measures.

# Short explanation of files

* tfsensor.once.py: Reads the measured data and writes the results in HTML format to standard output.
* tempcheck_web: Periodically calls `tfsensor.once.py` and pipes its output to a HTML file.
* tempcheck.service: SystemD service unit that starts `tempcheck_web`.

# Installation

`tempcheck.service` must reside in `/etc/systemd/system`. If it should be started at boot time, `systemctl enable tempcheck` must be executed once.