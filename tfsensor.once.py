# -*- coding: utf-8 -*
# Developed at #glt18 (FH Joanneum Graz Eggenberg) on 2018/04/27
import datetime
from time import sleep
from dhtxx import DHT22
from gpiozero import TrafficLights
from gpiozero import LED

def SetRYG(red, yellow, green):
	if not green:
		lights.green.off()
	else:
		lights.green.on()
	if not red:
		lights.red.off()
	else:
		lights.red.on()
	if not yellow:
		lights.yellow.off()
	else:
		lights.yellow.on()

dht = DHT22(4)
lights = TrafficLights(16, 20, 21)
#print("Reading data...")
ledstate = True
led = LED(17)
humidity = False
print("<html><head><title>Raspberry PI Zero Temperature Check</title>")
print("<meta http-equiv='refresh' content='10'>")
print("</head><style>P { font-size: 200%; }</style><body>")
doit = True
pgreen = "<p style='color: Green;'>"
pred = "<p style='color: Red;'>"
while doit:
#	ledstate = not ledstate
#	if ledstate:
	led.on()
#	else:
#		led.off()
	try:
		res = dht.get_result_once()
		led.off()
		print('<h3>Data gathered at ' + datetime.datetime.now().isoformat(' ') + '</h3>')
		print('<p>Temperature: ' + '{:.2f}'.format(res[0]) + ' &deg;C <br/>Humidity: ' + '{:.2f}'.format(res[1]) + ' %</p>')
		if res[1] > 80 or res[1] < 40:
			humidity = True
		else:
			humidity = False
		if res[0] < 24:
			print(pgreen + "Temperature is okay</p>")
			SetRYG(False, humidity, True)
		else:
			print(pred + "Temperature is too high</p>")
			SetRYG(True, humidity, False)
		if humidity:
			print(pred + "Humidity is not between 40% and 80%</p>")
		else:
			print(pgreen + "Humidity is between 40% and 80%</p>")
	except Exception as e:
		pass
	sleep(1)
	doit = False

print("</body></html>")
